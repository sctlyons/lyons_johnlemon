﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public Text moneyText;

    public bool ending = false;

    public GameEnding gameEnding;

    private int money;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        money = 0;
        SetMoneyText();
        moneyText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize(); //normalizes movement vector
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f); //determines if there is horizantol input
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f); //determines if there is vertical input
        bool isWalking = hasHorizontalInput || hasVerticalInput; //if there is horizontal or vertical input isWalk = true
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f); //calculates the character's forward vector according to player's movement
        m_Rotation = Quaternion.LookRotation(desiredForward);
        SetMoneyText();
        if (ending == true)
        {
            
        }

    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Treasure"))
        {
            Destroy(other.gameObject);
            money = money + 100;
            SetMoneyText ();
        }
        
    }
    void SetMoneyText()
    {
        moneyText.text = "Earnings: $ " + money.ToString();
        if (money >= 500)
        {
            ending = true;
            gameEnding.WinGame();
        }

    }
}
