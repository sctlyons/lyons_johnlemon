﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            GhostRespawn gr = other.transform.parent.GetComponent<GhostRespawn>();
            gr.Invoke("respawn", 7f);

            other.gameObject.SetActive(false);
        }
    }
}
