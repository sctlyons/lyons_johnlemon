﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PickupSeeker : MonoBehaviour
{
    public Text enemyMoney;
    public GameEnding gameEnding;

    public bool ending = false;

    private int money;

    public NavMeshAgent navMeshAgent;

    public Transform[] pickups;
    Transform targetPickup;
    bool stopped;

    void Start()
    {
        money = 0;
        SetEnemyMoneyText();
        enemyMoney.text = "";
    }
    void Update()
    {
        if (!stopped)
        {
            if (targetPickup != null)
            {
                navMeshAgent.SetDestination(targetPickup.position);
            }
            else
                FindNearest();
        }
        SetEnemyMoneyText();
        if (ending == true)
        {

        }
    }

    void FindNearest()
    {
        int nearest = -1;
        float nearestDist = 1000000;
        for (int i = 0; i < pickups.Length; i++)
        {
            if (pickups[i] != null)
            {
                float dist = Vector3.Distance(pickups[i].position, transform.position);
                if (dist < nearestDist)
                {
                    nearest = i;
                    nearestDist = dist;
                }
            }
        }
        if (nearest !=-1 && pickups[nearest] != null)
        {
            targetPickup = pickups[nearest];
        }
        else
        {
            stopped = true;
            navMeshAgent.isStopped = true;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Treasure"))
        {
            Destroy(other.gameObject);
            money = money + 100;
            SetEnemyMoneyText();
        }
    }
    void SetEnemyMoneyText()
    {
        enemyMoney.text = "Kid's stolen earnings: $" + money.ToString();
        if (money >= 500)
        {
            ending = true;
        }

    }
}
